;;; frct.el --- Not so stiff time tracking -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mihail Iosilevitch

;; Author: Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;; Version: 0.2
;; Keywords: calendar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; See full description of this system:
;; https://www.lesswrong.com/posts/RWu8eZqbwgB9zaerh

;;;; Usage
;; Main entry point is `frct-switch' function.
;; Sample session:
;; 0. frct.el is inactive
;; 1. Run ~frct-switch~ to start *work* period
;; 2. Start working
;; 3. Run ~frct-switch~ again to toggle to *break* period
;; 4. Take a break
;; 5. Emacs will notify you when *break* ends and ask if it should start
;; *work* again (goto 2) or turn timer off (goto 0)

;; Also you can run ~frct-switch~ in *break* period to start *work*
;; earlier. "Unused" break time will be postponed and next *break* will
;; be longer.

;;;; Why the name "frct"?
;; "fraction"

;;; Code:

(defgroup frct nil
  "Not so stiff time tracking system.

Similar to pomodoro, but lengths of work and break periods are not fixed.
See: https://www.lesswrong.com/posts/RWu8eZqbwgB9zaerh/third-time-a-better-way-to-work"
  :prefix "frct-"
  :group 'applications)

;;;; Custom variables

(defcustom frct-fraction (/ 1.0 3.0)
  "A fraction which break time consist of work."
  :group 'frct
  :type 'float)

(defcustom frct-time-format "%.2m:%.2s"
  "Format for the time reperesentation.

The format is the same as in `format-seconds'."
  :group 'frct
  :type 'string)

(defcustom frct-tick-hook nil
  "List of hook functions run each second when frct timer is active."
  :group 'frct
  :type 'hook)

(defcustom frct-break-start-functions nil
  "List of special hook functions run after break start.

Each function is called with one argument, break length in
seconds."
  :group 'frct
  :type 'hook)

(defcustom frct-work-start-functions nil
  "List of special hook functions run after work start.

Each function is called with one argument, postponed time in
seconds."
  :group 'frct
  :type 'hook)

(defcustom frct-reset-hook nil
  "List of hook functions run when `frct-reset' is called."
  :group 'frct
  :type 'hook)

;;;; Internal variables

(defvar frct--timer nil
  "A variable for frct.el timer.")

(defvar frct--state nil
  "Current state of frct.el.

A symbol, `work', `break' or nil.")

(defvar frct--period-start nil
  "Time (in seconds) at which current period started.")

(defvar frct--period-length nil
  "Length of current period (in seconds).

Only useful if variable `frct--state' have `break' value.")

(defvar frct--postponed-time 0
  "Postponed time for breaks (in seconds).

Will be apended at next break.")

(defvar frct-mode-line-string ""
  "String which displayed in modeline.

Show current period (work or break) and remaining time.")

;;;; Time calculations and logic

(defun frct--current-time ()
  "Helper function, return current time in seconds (like UNIX time)."
  (time-convert (current-time) 'integer))

(defun frct--current-period-length ()
  "Get current length of work period or remaining time in break period."
  (if (eq frct--state 'work)
      (- (frct--current-time) frct--period-start)
    (- frct--period-length
       (- (frct--current-time) frct--period-start))))

(defun frct--tick ()
  "A function for updating state, ran every second."
  (run-hooks 'frct-tick-hook)
  ;; Process end of break
  (when (and (eq 'break frct--state)
             (>= 0 (frct--current-period-length)))
    (frct--stop-timer)
    ;; This magic with let and `frct--period-start' is required to
    ;; prevent negative postponed time: `y-or-n-p' can spend a lot of
    ;; time waiting for an answer and we should not take it into account
    (let ((end-time (frct--current-time)))
      (if (y-or-n-p "Start work?")
          (progn
            (setq frct--period-start (+ frct--period-start
                                        (- (frct--current-time)
                                           end-time)))
            (frct-switch))
        (frct-reset)))))

;; NOTE: this snippet is very useful when several hundred of timers are
;; started accidentally: (cancel-function-timers #'frct--tick)

(defun frct--start-timer ()
  "Start timer for frct.el if not yet."
  (unless (timerp frct--timer)
    (setq frct--timer (run-with-timer 0 1 #'frct--tick))))

(defun frct--stop-timer ()
  "Stop timer for frct.el."
  (cancel-timer frct--timer)
  (setq frct--timer nil))

;;;; User-facing functions

(defun frct-update-mode-line-string ()
  "Update modeline for frct.el.

Sets the value of `frct-mode-line-string' according to
`frct--state' and `frct--period-length'."
  (setq frct-mode-line-string
        (if frct--state
            (format " [%S] %s"
                    frct--state
                    (format-seconds
                     frct-time-format
                     (frct--current-period-length)))
          ""))
  (force-mode-line-update t))

;;;###autoload
(defun frct-switch ()
  "Toggle between break and work periods.

If none of them are active, start work period."
  (interactive)
  (if (eq 'work frct--state)
      ;; Start break
      (progn
        (setq frct--period-length
              (round (+ (* frct-fraction
                           (frct--current-period-length))
                        frct--postponed-time)))
        (setq frct--postponed-time 0)
        (setq frct--state 'break)
        (setq frct--period-start (frct--current-time))
        (frct--start-timer)
        (run-hook-with-args 'frct-break-start-functions
                            frct--period-length))
    ;; Start work
    (when (eq 'break frct--state)
      (setq frct--postponed-time
            (+ frct--postponed-time
               (frct--current-period-length))))
    (run-hook-with-args 'frct-work-start-functions frct--postponed-time)
    (setq frct--state 'work)
    (setq frct--period-start (frct--current-time))
    (setq frct--period-length nil)
    (frct--start-timer)))

(defun frct-reset ()
  "Reset current state, disable everything."
  (interactive)
  (run-hooks 'frct-reset-hook)
  (setq frct--state nil)
  (setq frct--period-start nil)
  (setq frct--period-length nil)
  (setq frct--postponed-time 0)
  (when (timerp frct--timer)
    (frct--stop-timer))
  (frct-update-mode-line-string))

;;; Metadata
(provide 'frct)
;;; frct.el ends here
